MPD - Multiplex PCR Design data files
===========================================

by Thomas Wingo and David Cutler

## Description

These are the data files needed for the mpd project. Split from the main `mpd-c` and `mpd-perl` repos due to size.

## Installation

Clone these into a separate directry and link them into the directory running mpd.


## Creation

These files were created using fasta genome and flat dbSNP files from UCSC genome browser.
